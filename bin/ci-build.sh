#!/bin/bash
set -e

adoc_dir=/opt/adoc-reveal

# copy default css/images to slider folder
mkdir public
cp -av $adoc_dir/css public/
cp -av $adoc_dir/images public/
cp -a $adoc_dir/reveal.js public/

cp -av ./slides/css public/
cp -av ./slides/images public/

asciidoctor-revealjs -r asciidoctor-diagram --destination-dir public ./slides/*.adoc
