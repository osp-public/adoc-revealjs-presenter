#!/bin/bash
set -e

# copy default css/images to slider folder
cp -Rv css /slides/html5/
cp -Rv images /slides/html5/
cp -R reveal.js /slides/html5/

cp -Rv /slides/css /slides/html5/
cp -Rv /slides/images /slides/html5/

./bin/reval-build.sh /slides/*.adoc

