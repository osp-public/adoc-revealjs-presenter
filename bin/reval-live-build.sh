#!/bin/bash
set -e

./bin/reval-prepare_and_build.sh

while true ; do 
  adoc_file=`inotifywait --format=%w /slides/*.adoc`
  ./bin/reval-build.sh $adoc_file
done
