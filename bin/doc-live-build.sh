#!/bin/bash
set -e

mkdir -p /doc/html
./bin/doc-build.sh /doc/*.adoc

while true ; do 
  adoc_file=`inotifywait --format=%w /doc/*.adoc`
  ./bin/doc-build.sh $adoc_file
done
