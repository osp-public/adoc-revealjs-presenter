#!/bin/sh
echo "$(date -Iseconds) Processing $*"
asciidoctor -r asciidoctor-diagram --destination-dir /doc/html $*
