FROM ruby:3.1

RUN apt-get update && apt-get upgrade -y
RUN apt-get install -y inotify-tools graphviz curl
# aktuell läuft bei Debian der erste Aufruf auf einen Fehler, beim Zweiten geht es dann, schräg
RUN apt-get install -y plantuml || apt-get install -y plantuml
RUN mkdir -p /opt/adoc-reveal && mkdir -p /slides/html5
WORKDIR /opt/adoc-reveal

COPY Gemfile .
RUN bundle install

# download new version of plantuml.jar
RUN curl https://github.com/plantuml/plantuml/releases/download/v1.2023.5/plantuml-1.2023.5.jar -o /usr/share/plantuml/plantuml.jar -L

COPY . .

RUN date -R > build-time.txt

CMD ./bin/reval-live-build.sh
